const place = document.querySelector("body");
const table = document.createElement("table");

function makeTable() {
  table.setAttribute("border", "5px");
  table.setAttribute("style", "background-color: white;");
  let rows = 30;
  let columns = 30;
  for (var i = 0; i < rows; i++) {
    let tr = document.createElement("tr");
    for (var j = 0; j < columns; j++) {
      let td = document.createElement("td");
      let text = document.createTextNode(i + 1 + "." + (j + 1));
      td.appendChild(text);
      tr.appendChild(td);
      td.setAttribute("align", "center");
      td.setAttribute("width", "50px");
      td.setAttribute("height", "30px");
    }
    table.appendChild(tr);
  }
  place.appendChild(table);
}

makeTable.currentColor = function() {
  let color = table.getAttribute("style", "background-color");
  console.log(color);
};

class Some {
  constructor(selector) {
    this.elems = document.querySelectorAll(selector);
  }
  on(event, callback) {
    for (var i = 0; i < this.elems.length; i++) {
      this.elems[i].addEventListener(event, callback);
    }
    return this;
  }
}

makeTable();
makeTable.currentColor();

//const some = new Some("tr td");//изменение цвета ячеек
//console.log(some);

//some.on("click", function() {
//this.style.background === "red"
//? (this.style.background = "black")
//: (this.style.background = "red");
//});

place.addEventListener("click", function() {
  if (event.target === document.body) {
    toggleColor();
  }
  if (event.target.tagName === "TD") {
    let isBlack = event.target.classList.contains("black-cell");
    let isRed = event.target.classList.contains("red-cell");
    !isBlack && !isRed
      ? event.target.classList.toggle("red-cell")
      : event.target.classList.toggle("black-cell");
    console.log(event.target.getAttribute("class"), isRed, isBlack);
  }
});

function toggleColor() {
  console.log("Clicked on Body");
  let cellsBody = document.body.querySelectorAll(".red-cell");
  for (let i = 0; i < cellsBody.length; i++) {
    cellsBody[i].classList.toggle("black-cell");
  }
  console.log(cellsBody);
  return cellsBody;
}
